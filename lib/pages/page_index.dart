import 'package:flutter/material.dart';

class PageIndex extends StatelessWidget {
  const PageIndex({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text(
          '우리집 백수',
          style: TextStyle(
            fontFamily: "font_02",
          ),
        ),
        leading: IconButton(
          onPressed: () {},
          icon: Icon(Icons.menu),
        ),
        backgroundColor: Colors.blue,
        foregroundColor: Colors.white,
      ),
      drawer: Drawer(
        child: ListView(
        ),
      ),

      body:
      SingleChildScrollView(
        child: Column(
          children: [
            Image.asset(
                'assets/cat.jpg',
              width: 500,
              height: 500,
            ),
            Container(
              child: Column(
                children: [
                  Text(
                      '< 백수를 소개합니다 >',
                    style: TextStyle(
                      fontFamily: "font_03",
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      height: 3,
                      color: Colors.blue
                    ),
                  ),
                  Text(
                      '이 고양이는 백수입니다.',
                    style: TextStyle(
                      fontFamily: "font_02",
                      color: Colors.grey
                    )
                  ),
                  Text(
                      '그래도 귀엽습니다.',
                      style: TextStyle(
                        fontFamily: "font_02",
                        color: Colors.grey
                      )
                  ),
                ],
              ),
              padding: EdgeInsets.symmetric(vertical: 20),
            ),
            Container(
              child: Column(
                children: [
                  Row(
                    children: [
                      Image.asset(
                          'assets/cat_01.jpg',
                        width: 300,
                        height: 300,
                      ),
                      Image.asset(
                          'assets/cat_02.jpg',
                        width: 300,
                        height: 300,
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Image.asset(
                          'assets/cat_03.jpg',
                        width: 300,
                        height: 300,
                      ),
                      Image.asset(
                          'assets/cat_04.jpg',
                        width: 300,
                        height: 300,
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}